import java.io.*;
import java.util.*;

/**
 * Fighter
 */
public class Fighter {

    private int mBaseHp;
    private int mWp;

    public double getCombatScore()
    {
        return 0;
    }

    Fighter(int BaseHp, int Wp) {
        this.mBaseHp = BaseHp;
        this.mWp = Wp;
    }

    public double getBaseHp() {
        return this.mBaseHp;
    }

    public void setBaseHp(int baseHp) {
        this.mBaseHp = baseHp;
    }

    public int getWp() {
        return this.mWp;
    }

    public void setWp(int wp) {
        this.mWp = wp;
    }
}