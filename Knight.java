import java.io.*;

/**
 * Knight
 */
public class Knight extends Fighter {

    Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    public double getCombatScore() {
        double CombatScore = 0;
        // Square
        if (Utility.isSquare(Utility.Ground)) {
            CombatScore = 2 * getBaseHp();
            // Normal
        } else if (!Utility.isPrime(Utility.Ground) && !Utility.isSquare(Utility.Ground)) {
            if (Utility.Ground == 999) {
                double reach_point = 2 * getBaseHp();

                // Space-optimize Fibonacci
                int a = 0;
                int b = 1;
                int c;
                while (b <= reach_point) {
                    c = a + b;
                    a = b;
                    b = c;
                }

                CombatScore = b;
            } else if (this.getWp() == 1) {
                CombatScore = getBaseHp();
            } else {
                CombatScore = getBaseHp() / 10;
            }
        }
        // Prime
        else if (Utility.isPrime(Utility.Ground)) {
            if (this.getWp() == 1)
                CombatScore = getBaseHp();
            else
                CombatScore = getBaseHp() / 10;
        }
        return CombatScore;
    }
}