import java.io.*;

public class Test_Fighter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Knight k = new Knight(500, 0);
		Warrior w = new Warrior(400, 1);
		Utility.Ground = 999;
		System.out.println("Knight's combat score: " + k.getCombatScore());
		System.out.println("Warrior's combat score: " + w.getCombatScore());
		System.out.println();
		Utility.Ground = 67;
		System.out.println("Knight's combat score: " + k.getCombatScore());
		System.out.println("Warrior's combat score: " + w.getCombatScore());
		System.out.println();
		Utility.Ground = 36;
		System.out.println("Knight's combat score: " + k.getCombatScore());
		System.out.println("Warrior's combat score: " + w.getCombatScore());
		System.out.println();
		Utility.Ground = 10;
		System.out.println("Knight's combat score: " + k.getCombatScore());
		System.out.println("Warrior's combat score: " + w.getCombatScore());
		System.out.println();
	}
}
