import java.io.*;

/**
 * Warrior
 */
public class Warrior extends Fighter {

    Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }

    public double getCombatScore() {
        double CombatScore = 0;
        if (Utility.isPrime(Utility.Ground)) {
            CombatScore = 2 * getBaseHp();
        } else {
            if (getWp() == 1) {
                CombatScore = getBaseHp();
            } else
                CombatScore = getBaseHp() / 10;
        }
        return CombatScore;
    }
}